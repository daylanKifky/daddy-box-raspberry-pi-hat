Daddy Box
=========
_Raspberry Pi B+ Hat to build simplified, button-based interface for telegram voice chat for kids._

See [this article](https://chordata.cc/blog/open-source-intercom-for-kids/) for details and building instructions.

![daddy box HAT](https://chordata.cc/wp-content/uploads/2021/01/daddy_box_HAT-640x425.jpg)

## Soldering info

The components can be found on the `BOM.csv` file.


## Power selection

The HAT is prepeared to accept different voltage inputs to potentialy handle bigger loads though the `POWER screw` and `POWER  jack` connectors. These connectors are not used in the telegram intercom project, so you can just leave them empty.

Instead short the terminals of the `RPi5v` jumper to use the 5V rail from the raspberry. This will be enough to handle most arcade button leds.


## Wiring

In this project 5 arcade backlighted buttons are used. They normally come with 4 contacts: two for the button and two for the led. Use a 3V battery to identify the anode and cathode of the led contacts

### Buttons wiring 

- One contact of each button should go to the `GND OUT` header
- The other end of the 4 white buttons go to the `BTN CONN` positions 1 to 4
- The red button should go to position 5 of the `BTN CONN`

### Leds wiring 

- The anode of each led should go to the `VCC OUT` header
- The cathode of the four white leds go to the `LED CONN` positions 1 to 4
- The cathode of the red led go to position 5 of the `BTN CONN`


## KICAD components

The component footprints used in this template are [here](https://github.com/xesscorp/RPi_Hat.pretty).

Credits
-------

Based on [RPi HAT template by XESS Corp](https://github.com/xesscorp/RPi_Hat_Template.git)
