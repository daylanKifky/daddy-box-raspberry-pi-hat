EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RPi_Hat-rescue:RPi_GPIO J2
U 1 1 5516AE26
P 7500 2700
AR Path="/5516AE26" Ref="J2"  Part="1" 
AR Path="/5515D395/5516AE26" Ref="J2"  Part="1" 
F 0 "J2" H 8250 2950 60  0000 C CNN
F 1 "RPi_GPIO" H 8250 2850 60  0000 C CNN
F 2 "RPi_Hat:Pin_Header_Straight_2x20" H 7500 2700 60  0001 C CNN
F 3 "" H 7500 2700 60  0000 C CNN
	1    7500 2700
	1    0    0    -1  
$EndComp
Text Notes 7800 5000 0    60   Italic 0
Thru-Hole Connector
$Comp
L Transistor_Array:ULN2003 U1
U 1 1 5FB15831
P 4800 3050
F 0 "U1" H 4800 3717 50  0000 C CNN
F 1 "ULN2003" H 4800 3626 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4850 2500 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 4900 2850 50  0001 C CNN
	1    4800 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J6
U 1 1 5FB17FCB
P 1400 3600
F 0 "J6" H 1318 3275 50  0000 C CNN
F 1 "VIN screw" H 1318 3366 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1400 3600 50  0001 C CNN
F 3 "~" H 1400 3600 50  0001 C CNN
	1    1400 3600
	-1   0    0    1   
$EndComp
Text GLabel 1600 3500 2    50   Input ~ 0
GND
Text GLabel 1650 4200 2    50   Input ~ 0
GND
$Comp
L Connector:Barrel_Jack_Switch J7
U 1 1 5FB1C08C
P 1350 4100
F 0 "J7" H 1407 4417 50  0000 C CNN
F 1 "VIN Jack" H 1407 4326 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-102AH_Horizontal" H 1400 4060 50  0001 C CNN
F 3 "~" H 1400 4060 50  0001 C CNN
	1    1350 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP3
U 1 1 5FB1E392
P 2500 3600
F 0 "JP3" H 2500 3785 50  0000 C CNN
F 1 "JP screw" H 2500 3694 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2500 3600 50  0001 C CNN
F 3 "~" H 2500 3600 50  0001 C CNN
	1    2500 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP4
U 1 1 5FB1EBE5
P 2500 4000
F 0 "JP4" H 2500 4185 50  0000 C CNN
F 1 "JP jack" H 2500 4100 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2500 4000 50  0001 C CNN
F 3 "~" H 2500 4000 50  0001 C CNN
	1    2500 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP2
U 1 1 5FB1EEF7
P 2500 3000
F 0 "JP2" H 2500 3185 50  0000 C CNN
F 1 "JP RPI 3v3" H 2500 3094 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2500 3000 50  0001 C CNN
F 3 "~" H 2500 3000 50  0001 C CNN
	1    2500 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 5FB1F40A
P 2500 2500
F 0 "JP1" H 2500 2685 50  0000 C CNN
F 1 "JP RPI-5v" H 2500 2594 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2500 2500 50  0001 C CNN
F 3 "~" H 2500 2500 50  0001 C CNN
	1    2500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3600 2400 3600
Wire Wire Line
	2400 4000 1650 4000
Text GLabel 1550 3000 0    50   Input ~ 0
RPI-3v3
Text GLabel 9200 2700 2    50   Input ~ 0
RPI-5v
Text GLabel 1550 2500 0    50   Input ~ 0
RPI-5v
Text GLabel 7300 2700 0    50   Input ~ 0
RPI-3v3
$Comp
L power:GND #PWR0101
U 1 1 5FB25ADE
P 10350 3000
F 0 "#PWR0101" H 10350 2750 50  0001 C CNN
F 1 "GND" H 10355 2827 50  0000 C CNN
F 2 "" H 10350 3000 50  0001 C CNN
F 3 "" H 10350 3000 50  0001 C CNN
	1    10350 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3000 10350 2900
Wire Wire Line
	10350 2900 9200 2900
Wire Wire Line
	2400 2500 1550 2500
Wire Wire Line
	2400 3000 1550 3000
Text GLabel 3550 3200 2    50   Input ~ 0
VCC
Wire Wire Line
	2600 2500 3300 2500
Wire Wire Line
	3300 2500 3300 3000
Wire Wire Line
	3300 4000 2600 4000
Wire Wire Line
	2600 3600 3300 3600
Connection ~ 3300 3600
Wire Wire Line
	3300 3600 3300 4000
Wire Wire Line
	2600 3000 3300 3000
Connection ~ 3300 3000
Wire Wire Line
	3300 3000 3300 3200
Wire Wire Line
	3550 3200 3450 3200
Connection ~ 3300 3200
Wire Wire Line
	3300 3200 3300 3600
Wire Wire Line
	10350 2900 10550 2900
Connection ~ 10350 2900
Text GLabel 10550 2900 2    50   Input ~ 0
GND
$Comp
L power:VCC #PWR0102
U 1 1 5FB3042B
P 3450 3200
F 0 "#PWR0102" H 3450 3050 50  0001 C CNN
F 1 "VCC" H 3465 3373 50  0000 C CNN
F 2 "" H 3450 3200 50  0001 C CNN
F 3 "" H 3450 3200 50  0001 C CNN
	1    3450 3200
	1    0    0    -1  
$EndComp
Connection ~ 3450 3200
Wire Wire Line
	3450 3200 3300 3200
NoConn ~ 1650 4100
Text GLabel 7300 3300 0    50   Input ~ 0
BTN1
Text GLabel 7300 3700 0    50   Input ~ 0
BTN2
Text GLabel 7300 4200 0    50   Input ~ 0
BTN3
Text GLabel 7300 4400 0    50   Input ~ 0
BTN4
Text GLabel 9200 3500 2    50   Input ~ 0
BTN5
Text GLabel 9200 4500 2    50   Input ~ 0
BTN6
Text GLabel 7300 3200 0    50   Input ~ 0
LED1
Text GLabel 7300 3600 0    50   Input ~ 0
LED2
Text GLabel 7300 4100 0    50   Input ~ 0
LED3
Text GLabel 7300 4300 0    50   Input ~ 0
LED4
Text GLabel 9200 3400 2    50   Input ~ 0
LED5
Text GLabel 9200 4400 2    50   Input ~ 0
LED6
Text GLabel 4400 2850 0    50   Input ~ 0
LED1
Text GLabel 4400 2950 0    50   Input ~ 0
LED2
Text GLabel 4400 3050 0    50   Input ~ 0
LED3
Text GLabel 4400 3150 0    50   Input ~ 0
LED4
Text GLabel 4400 3250 0    50   Input ~ 0
LED5
Text GLabel 4400 3350 0    50   Input ~ 0
LED6
$Comp
L power:GND #PWR0103
U 1 1 5FB1DA94
P 4800 3800
F 0 "#PWR0103" H 4800 3550 50  0001 C CNN
F 1 "GND" H 4805 3627 50  0000 C CNN
F 2 "" H 4800 3800 50  0001 C CNN
F 3 "" H 4800 3800 50  0001 C CNN
	1    4800 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3650 4800 3800
Text GLabel 6000 2650 2    50   Input ~ 0
LED_PWR_1
Text GLabel 6000 2750 2    50   Input ~ 0
LED_PWR_2
Text GLabel 6000 3150 2    50   Input ~ 0
LED_PWR_3
Text GLabel 6000 3250 2    50   Input ~ 0
LED_PWR_4
Text GLabel 6000 3600 2    50   Input ~ 0
LED_PWR_5
Text GLabel 6000 3700 2    50   Input ~ 0
LED_PWR_6
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 5FB2197B
P 1150 4900
F 0 "J8" V 1400 4850 50  0000 C CNN
F 1 "GND OUT" V 1300 4850 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1150 4900 50  0001 C CNN
F 3 "~" H 1150 4900 50  0001 C CNN
	1    1150 4900
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J9
U 1 1 5FB2458E
P 1900 4900
F 0 "J9" V 2150 4850 50  0000 C CNN
F 1 "VCC OUT" V 2050 4850 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 1900 4900 50  0001 C CNN
F 3 "~" H 1900 4900 50  0001 C CNN
	1    1900 4900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5FB25145
P 1250 5250
F 0 "#PWR0104" H 1250 5000 50  0001 C CNN
F 1 "GND" H 1255 5077 50  0000 C CNN
F 2 "" H 1250 5250 50  0001 C CNN
F 3 "" H 1250 5250 50  0001 C CNN
	1    1250 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5100 1250 5100
Wire Wire Line
	1250 5100 1250 5250
Connection ~ 1250 5100
Text GLabel 2150 5100 2    50   Input ~ 0
VCC
Wire Wire Line
	1900 5100 2000 5100
Wire Wire Line
	2000 5100 2150 5100
Connection ~ 2000 5100
$Comp
L Device:LED D1
U 1 1 5FB2A676
P 3550 850
F 0 "D1" H 3543 595 50  0000 C CNN
F 1 "LED_PILOTO_1" H 3543 686 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3550 850 50  0001 C CNN
F 3 "~" H 3550 850 50  0001 C CNN
	1    3550 850 
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5FB2BD2C
P 4050 850
F 0 "R1" V 3843 850 50  0000 C CNN
F 1 "R" V 3934 850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3980 850 50  0001 C CNN
F 3 "~" H 4050 850 50  0001 C CNN
	1    4050 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 850  3900 850 
Text GLabel 3150 850  0    50   Input ~ 0
LED1
Wire Wire Line
	3150 850  3400 850 
$Comp
L Device:LED D2
U 1 1 5FB2FD1A
P 3550 1250
F 0 "D2" H 3543 995 50  0000 C CNN
F 1 "LED_PILOTO_2" H 3543 1086 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3550 1250 50  0001 C CNN
F 3 "~" H 3550 1250 50  0001 C CNN
	1    3550 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5FB2FD98
P 4050 1250
F 0 "R2" V 3843 1250 50  0000 C CNN
F 1 "R" V 3934 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3980 1250 50  0001 C CNN
F 3 "~" H 4050 1250 50  0001 C CNN
	1    4050 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 1250 3900 1250
Text GLabel 3150 1250 0    50   Input ~ 0
LED2
Wire Wire Line
	3150 1250 3400 1250
$Comp
L Device:LED D3
U 1 1 5FB3290F
P 3550 1650
F 0 "D3" H 3543 1395 50  0000 C CNN
F 1 "LED_PILOTO_3" H 3543 1486 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3550 1650 50  0001 C CNN
F 3 "~" H 3550 1650 50  0001 C CNN
	1    3550 1650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5FB32A55
P 4050 1650
F 0 "R3" V 3843 1650 50  0000 C CNN
F 1 "R" V 3934 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3980 1650 50  0001 C CNN
F 3 "~" H 4050 1650 50  0001 C CNN
	1    4050 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 1650 3900 1650
Text GLabel 3150 1650 0    50   Input ~ 0
LED3
Wire Wire Line
	3150 1650 3400 1650
$Comp
L Device:LED D4
U 1 1 5FB3B2AC
P 5350 850
F 0 "D4" H 5343 595 50  0000 C CNN
F 1 "LED_PILOTO_4" H 5343 686 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5350 850 50  0001 C CNN
F 3 "~" H 5350 850 50  0001 C CNN
	1    5350 850 
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5FB3B41A
P 5850 850
F 0 "R4" V 5643 850 50  0000 C CNN
F 1 "R" V 5734 850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 850 50  0001 C CNN
F 3 "~" H 5850 850 50  0001 C CNN
	1    5850 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 850  5700 850 
Text GLabel 4950 850  0    50   Input ~ 0
LED4
Wire Wire Line
	4950 850  5200 850 
$Comp
L Device:LED D5
U 1 1 5FB3B427
P 5350 1250
F 0 "D5" H 5343 995 50  0000 C CNN
F 1 "LED_PILOTO_5" H 5343 1086 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5350 1250 50  0001 C CNN
F 3 "~" H 5350 1250 50  0001 C CNN
	1    5350 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5FB3B431
P 5850 1250
F 0 "R5" V 5643 1250 50  0000 C CNN
F 1 "R" V 5734 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 1250 50  0001 C CNN
F 3 "~" H 5850 1250 50  0001 C CNN
	1    5850 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 1250 5700 1250
Text GLabel 4950 1250 0    50   Input ~ 0
LED5
Wire Wire Line
	4950 1250 5200 1250
$Comp
L Device:LED D6
U 1 1 5FB3B43E
P 5350 1650
F 0 "D6" H 5343 1395 50  0000 C CNN
F 1 "LED_PILOTO_6" H 5343 1486 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5350 1650 50  0001 C CNN
F 3 "~" H 5350 1650 50  0001 C CNN
	1    5350 1650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5FB3B448
P 5850 1650
F 0 "R6" V 5643 1650 50  0000 C CNN
F 1 "R" V 5734 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 1650 50  0001 C CNN
F 3 "~" H 5850 1650 50  0001 C CNN
	1    5850 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	5500 1650 5700 1650
Text GLabel 4950 1650 0    50   Input ~ 0
LED6
Wire Wire Line
	4950 1650 5200 1650
$Comp
L power:GND #PWR0105
U 1 1 5FB40FA4
P 4450 1900
F 0 "#PWR0105" H 4450 1650 50  0001 C CNN
F 1 "GND" H 4455 1727 50  0000 C CNN
F 2 "" H 4450 1900 50  0001 C CNN
F 3 "" H 4450 1900 50  0001 C CNN
	1    4450 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5FB453C1
P 6400 1850
F 0 "#PWR0106" H 6400 1600 50  0001 C CNN
F 1 "GND" H 6405 1677 50  0000 C CNN
F 2 "" H 6400 1850 50  0001 C CNN
F 3 "" H 6400 1850 50  0001 C CNN
	1    6400 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1900 4450 1650
Wire Wire Line
	4450 850  4200 850 
Wire Wire Line
	4200 1250 4450 1250
Connection ~ 4450 1250
Wire Wire Line
	4450 1250 4450 850 
Wire Wire Line
	4200 1650 4450 1650
Connection ~ 4450 1650
Wire Wire Line
	4450 1650 4450 1250
Wire Wire Line
	6400 1850 6400 1650
Wire Wire Line
	6400 850  6000 850 
Wire Wire Line
	6000 1250 6400 1250
Connection ~ 6400 1250
Wire Wire Line
	6400 1250 6400 850 
Wire Wire Line
	6000 1650 6400 1650
Connection ~ 6400 1650
Wire Wire Line
	6400 1650 6400 1250
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 5FB5425E
P 8550 1250
F 0 "J4" H 8468 925 50  0000 C CNN
F 1 "LED_HEADER_B" H 8600 1050 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 8550 1250 50  0001 C CNN
F 3 "~" H 8550 1250 50  0001 C CNN
	1    8550 1250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5FB547F8
P 8550 750
F 0 "J1" H 8468 425 50  0000 C CNN
F 1 "LED_HEADER_A" H 8600 550 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 8550 750 50  0001 C CNN
F 3 "~" H 8550 750 50  0001 C CNN
	1    8550 750 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 5FB52B3E
P 8550 1700
F 0 "J5" H 8468 1375 50  0000 C CNN
F 1 "LED_HEADER_C" H 8600 1500 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 8550 1700 50  0001 C CNN
F 3 "~" H 8550 1700 50  0001 C CNN
	1    8550 1700
	1    0    0    -1  
$EndComp
Text GLabel 7800 750  0    50   Input ~ 0
LED_PWR_1
Text GLabel 7800 850  0    50   Input ~ 0
LED_PWR_2
Text GLabel 7800 1250 0    50   Input ~ 0
LED_PWR_3
Text GLabel 7800 1350 0    50   Input ~ 0
LED_PWR_4
Text GLabel 7800 1700 0    50   Input ~ 0
LED_PWR_5
Text GLabel 7800 1800 0    50   Input ~ 0
LED_PWR_6
$Comp
L Connector:Conn_01x06_Female J3
U 1 1 5FB76217
P 9950 950
F 0 "J3" H 9978 926 50  0000 L CNN
F 1 "LED_HEADER_ALL" H 9978 835 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9950 950 50  0001 C CNN
F 3 "~" H 9950 950 50  0001 C CNN
	1    9950 950 
	1    0    0    -1  
$EndComp
Text GLabel 9500 750  0    50   Input ~ 0
LED_PWR_1
Text GLabel 9500 850  0    50   Input ~ 0
LED_PWR_2
Text GLabel 9500 950  0    50   Input ~ 0
LED_PWR_3
Text GLabel 9500 1050 0    50   Input ~ 0
LED_PWR_4
Text GLabel 9500 1150 0    50   Input ~ 0
LED_PWR_5
Text GLabel 9500 1250 0    50   Input ~ 0
LED_PWR_6
Wire Wire Line
	9500 750  9750 750 
Wire Wire Line
	9500 850  9750 850 
Wire Wire Line
	9500 950  9750 950 
Wire Wire Line
	9500 1050 9750 1050
Wire Wire Line
	9500 1150 9750 1150
Wire Wire Line
	9500 1250 9750 1250
$Comp
L Connector:Screw_Terminal_01x02 J11
U 1 1 5FB94258
P 4700 5450
F 0 "J11" H 4618 5125 50  0000 C CNN
F 1 "BTN_HEADER_B" H 4618 5216 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 4700 5450 50  0001 C CNN
F 3 "~" H 4700 5450 50  0001 C CNN
	1    4700 5450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J10
U 1 1 5FB9459C
P 4700 4950
F 0 "J10" H 4618 4625 50  0000 C CNN
F 1 "BTN_HEADER_A" H 4618 4716 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 4700 4950 50  0001 C CNN
F 3 "~" H 4700 4950 50  0001 C CNN
	1    4700 4950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J13
U 1 1 5FB945A6
P 4700 5900
F 0 "J13" H 4618 5575 50  0000 C CNN
F 1 "BTN_HEADER_C" H 4618 5666 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 4700 5900 50  0001 C CNN
F 3 "~" H 4700 5900 50  0001 C CNN
	1    4700 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4950 4500 4950
Wire Wire Line
	3950 5050 4500 5050
Wire Wire Line
	3950 5450 4500 5450
Wire Wire Line
	3950 5550 4500 5550
Wire Wire Line
	3950 5900 4500 5900
Wire Wire Line
	3950 6000 4500 6000
Text GLabel 3950 4950 0    50   Input ~ 0
BTN1
Text GLabel 3950 5050 0    50   Input ~ 0
BTN2
Text GLabel 3950 5450 0    50   Input ~ 0
BTN3
Text GLabel 3950 5550 0    50   Input ~ 0
BTN4
Text GLabel 3950 5900 0    50   Input ~ 0
BTN5
Text GLabel 3950 6000 0    50   Input ~ 0
BTN6
$Comp
L Connector:Conn_01x06_Female J12
U 1 1 5FBA173B
P 6100 5550
F 0 "J12" H 6128 5526 50  0000 L CNN
F 1 "LED_HEADER_ALL" H 6128 5435 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 6100 5550 50  0001 C CNN
F 3 "~" H 6100 5550 50  0001 C CNN
	1    6100 5550
	1    0    0    -1  
$EndComp
Text GLabel 5550 5350 0    50   Input ~ 0
BTN1
Text GLabel 5550 5450 0    50   Input ~ 0
BTN2
Text GLabel 5550 5550 0    50   Input ~ 0
BTN3
Text GLabel 5550 5650 0    50   Input ~ 0
BTN4
Text GLabel 5550 5750 0    50   Input ~ 0
BTN5
Text GLabel 5550 5850 0    50   Input ~ 0
BTN6
Wire Wire Line
	5550 5350 5900 5350
Wire Wire Line
	5550 5450 5900 5450
Wire Wire Line
	5550 5550 5900 5550
Wire Wire Line
	5550 5650 5900 5650
Wire Wire Line
	5550 5750 5900 5750
Wire Wire Line
	5550 5850 5900 5850
$Comp
L Device:R R7
U 1 1 5FB6735E
P 5650 2650
F 0 "R7" V 5530 2650 50  0000 C CNN
F 1 "R" V 5530 2775 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 5580 2650 50  0001 C CNN
F 3 "~" H 5650 2650 50  0001 C CNN
	1    5650 2650
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5FB6D393
P 5650 2750
F 0 "R8" V 5735 2765 50  0000 C CNN
F 1 "R" V 5740 2885 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 5580 2750 50  0001 C CNN
F 3 "~" H 5650 2750 50  0001 C CNN
	1    5650 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5FB6D8E7
P 5650 3150
F 0 "R9" V 5555 3160 50  0000 C CNN
F 1 "R" V 5555 3250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 5580 3150 50  0001 C CNN
F 3 "~" H 5650 3150 50  0001 C CNN
	1    5650 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5FB6DD9B
P 5650 3250
F 0 "R10" V 5730 3255 50  0000 C CNN
F 1 "R" V 5730 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 5580 3250 50  0001 C CNN
F 3 "~" H 5650 3250 50  0001 C CNN
	1    5650 3250
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5FB6E1DF
P 5650 3600
F 0 "R11" V 5545 3600 50  0000 C CNN
F 1 "R" V 5545 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 5580 3600 50  0001 C CNN
F 3 "~" H 5650 3600 50  0001 C CNN
	1    5650 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5FB6E6FC
P 5650 3700
F 0 "R12" V 5740 3705 50  0000 C CNN
F 1 "R" V 5735 3815 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 5580 3700 50  0001 C CNN
F 3 "~" H 5650 3700 50  0001 C CNN
	1    5650 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	7800 750  8350 750 
Wire Wire Line
	7800 850  8350 850 
Wire Wire Line
	7800 1250 8350 1250
Wire Wire Line
	7800 1350 8350 1350
Wire Wire Line
	7800 1700 8350 1700
Wire Wire Line
	7800 1800 8350 1800
Wire Wire Line
	5200 2850 5350 2850
Wire Wire Line
	5350 2850 5350 2650
Wire Wire Line
	5350 2650 5500 2650
Wire Wire Line
	5200 2950 5400 2950
Wire Wire Line
	5400 2950 5400 2750
Wire Wire Line
	5400 2750 5500 2750
Wire Wire Line
	5200 3050 5400 3050
Wire Wire Line
	5400 3050 5400 3150
Wire Wire Line
	5400 3150 5500 3150
Wire Wire Line
	5200 3150 5350 3150
Wire Wire Line
	5350 3150 5350 3250
Wire Wire Line
	5350 3250 5500 3250
Wire Wire Line
	5200 3250 5300 3250
Wire Wire Line
	5300 3250 5300 3600
Wire Wire Line
	5300 3600 5500 3600
Wire Wire Line
	5200 3350 5250 3350
Wire Wire Line
	5250 3350 5250 3700
Wire Wire Line
	5250 3700 5500 3700
Wire Wire Line
	5800 2650 6000 2650
Wire Wire Line
	5800 2750 6000 2750
Wire Wire Line
	5800 3150 6000 3150
Wire Wire Line
	5800 3250 6000 3250
Wire Wire Line
	5800 3600 6000 3600
Wire Wire Line
	5800 3700 6000 3700
Text GLabel 9200 3300 2    50   Input ~ 0
GND
Text GLabel 9200 3600 2    50   Input ~ 0
GND
Text GLabel 9200 4100 2    50   Input ~ 0
GND
Text GLabel 9200 4300 2    50   Input ~ 0
GND
Text GLabel 7300 4600 0    50   Input ~ 0
GND
Text GLabel 7300 3900 0    50   Input ~ 0
GND
Text GLabel 7300 3100 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Female J14
U 1 1 5FBECBA8
P 9900 2100
F 0 "J14" H 9928 2076 50  0000 L CNN
F 1 "i2c Conn" H 9928 1985 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9900 2100 50  0001 C CNN
F 3 "~" H 9900 2100 50  0001 C CNN
	1    9900 2100
	1    0    0    -1  
$EndComp
Text GLabel 9700 2100 0    50   Input ~ 0
SDA
Text GLabel 9700 2200 0    50   Input ~ 0
SCL
Text GLabel 7300 2800 0    50   Input ~ 0
SDA
Text GLabel 7300 2900 0    50   Input ~ 0
SCL
Text GLabel 9200 4200 2    50   Input ~ 0
PWM
Text GLabel 7300 4500 0    50   Input ~ 0
BTN_X
$Comp
L Connector:Conn_01x02_Female J15
U 1 1 5FBFFAE2
P 9950 5450
F 0 "J15" H 9978 5426 50  0000 L CNN
F 1 "X Conn" H 9978 5335 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9950 5450 50  0001 C CNN
F 3 "~" H 9950 5450 50  0001 C CNN
	1    9950 5450
	1    0    0    -1  
$EndComp
Text GLabel 9750 5450 0    50   Input ~ 0
PWM
Text GLabel 9750 5550 0    50   Input ~ 0
BTN_X
$EndSCHEMATC
